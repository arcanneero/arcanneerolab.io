'use strict';

/** Imports Gulp */
const gulp = require('gulp');
const path = require('path');
const del = require('del');
const fs = require('fs');
const browserSync = require('browser-sync').create();
var rename = require("gulp-rename");
const $ = require('gulp-load-plugins')();
var Jimp = require('jimp');
var imageResize = require('gulp-image-resize');

/** Définition des constantes */

// Dossier des sources à builder
const srcDir = 'site';
// Dossier de sortie du build
const outDir = 'public';

// Constantes des extensions à prendre en compte pour les différents items du build
const mediasExtensions = [
    `${srcDir}/**/**/**/_images/*.{jpg,JPG}`,
    `${srcDir}/**/**/**/_images/**/*.{jpg,JPG}`
];
const siteFiles = [
    `${srcDir}/**`,
    `!${srcDir}/**/**/**/_images/*.{jpg,JPG}`,
    `!${srcDir}/**/**/**/_images/**/*.{jpg,JPG}`
];

gulp.task('resize-medias', () => {
    gulp.src(mediasExtensions)
        .pipe(imageResize({
            width : 800,
            quality : 30,
            crop : false
        }))
        .pipe(gulp.dest(outDir))
});

gulp.task('copy-medias', () => {
    gulp.src(mediasExtensions)
        .pipe(rename(function (path) {
            // Returns a completely new object, make sure you return all keys needed!
            return {
                dirname: path.dirname,
                basename: path.basename + "_full",
                extname: path.extname
            }}))
        .pipe(gulp.dest(outDir));
});

gulp.task('copy-site', () => {
    gulp.src(siteFiles).pipe(gulp.dest(outDir))
});


gulp.task('serveAndWatch', () => {
  browserSync.init({
    server: {
      baseDir: `./${outDir}/`
    },
    directory: true,
    notify: false,
    port: 3000
  });

  gulp.watch([srcDir + "/**"], () => $.sequence('copy-site', browserSync.reload));
  gulp.watch(mediasExtensions, () => $.sequence('resize-medias', browserSync.reload));
});


gulp.task('clean', () => del(outDir, { dot: true }));


// Build production files, the default task
gulp.task('default', cb =>
  $.sequence(
    'clean',
    ['copy-site', 'copy-medias', 'resize-medias'],
    cb
  )
);

// Build dev files
gulp.task('serve', cb => {
        $.sequence(
            ['copy-site', 'copy-medias'],
            'serveAndWatch',
            cb
        )
    }
);

